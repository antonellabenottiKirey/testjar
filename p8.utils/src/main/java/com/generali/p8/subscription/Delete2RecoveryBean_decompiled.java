package com.generali.p8.subscription;

import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.VersionSeries;
import com.filenet.api.engine.EventActionHandler;
import com.filenet.api.events.ObjectChangeEvent;
import com.filenet.api.events.Subscription;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.util.CmRecoveryBin;
import com.filenet.api.util.CmRecoveryItem;
import com.filenet.api.util.Id;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Delete2RecoveryBean_decompiled implements EventActionHandler {
	private static final String CLASS_NAME = Delete2RecoveryBean_decompiled.class.getName();
	private static final PropertyFilter PF_USER_STRING = new PropertyFilter();
	private Id sourceObjectId;
	private ObjectStore objectStore;
	private String logLevel = "Y";
	private String recoveryBinID = "{70F73652-BCEC-4397-A8B4-8984E74931E5}";
	private static final String version = "1.0.7";

	static {
		PF_USER_STRING.addIncludeProperty(1, (Long) null, (Boolean) null, "UserString", (Integer) null);
	}

	public void onEvent(ObjectChangeEvent objectChangeEvent, Id subscriptionId) throws EngineRuntimeException {
		long _start = resetTimer();
		this.sourceObjectId = objectChangeEvent.get_SourceObjectId();
		this.objectStore = objectChangeEvent.getObjectStore();
		this.logOut(" subscriptionId: " + subscriptionId.toString());

		try {
			String userString = this.getUserString(objectChangeEvent, subscriptionId);
			Document e = com.filenet.api.core.Factory.Document.fetchInstance(this.objectStore, this.sourceObjectId,
					(PropertyFilter) null);
			this.execute(this.objectStore, e, userString);
			this.logDebug("[Runnung time totale]: " + timerToString(this.getTimer(_start)));
		} catch (Exception arg6) {
			throw new EngineRuntimeException(ExceptionCode.E_FAILED, "Errore" + arg6.getMessage());
		}
	}

	public void execute(ObjectStore objectStore, Document sourceDocument, String userString) throws Exception {
		long _start = resetTimer();
		this.logDebug("[execute() Start]");
		this.findParameters(userString, objectStore, this.sourceObjectId);
		PropertyFilter propertyFilter = new PropertyFilter();
		propertyFilter.addIncludeProperty(new FilterElement((Integer) null, (Long) null, (Boolean) null,
				"RecoveryItems , DisplayName", (Integer) null));
		CmRecoveryBin cmRecoveryBin = com.filenet.api.core.Factory.CmRecoveryBin.fetchInstance(objectStore,
				new Id(this.recoveryBinID), propertyFilter);
		VersionSeries versionSeries = sourceDocument.get_VersionSeries();
		CmRecoveryItem markedItem = versionSeries.markForDeletion(cmRecoveryBin, "CmRecoveryItem");
		markedItem.save(RefreshMode.NO_REFRESH);
		this.logDebug("[execute() time]: " + timerToString(this.getTimer(_start)));
	}

	private String getUserString(ObjectChangeEvent objectChangeEvent, Id subscriptionId) throws Exception {
		long _start = resetTimer();
		this.logDebug("[getUserString() Start]");
		Subscription subscription = com.filenet.api.core.Factory.Subscription
				.fetchInstance(objectChangeEvent.getObjectStore(), subscriptionId, PF_USER_STRING);
		this.logDebug("[getUserString() time]: " + timerToString(this.getTimer(_start)));
		return subscription.get_UserString();
	}

	private void findParameters(String userString, ObjectStore objectStore, Id sourceObjectId) throws Exception {
		long _start = resetTimer();
		this.logDebug("[findParameters() Start]");
		this.logLevel = "Y";
		if (userString.trim().length() > 0) {
			List parameterist = Arrays.asList(userString.split(";"));
			Iterator iterator = parameterist.iterator();

			while (iterator.hasNext()) {
				String string = (String) iterator.next();
				String[] parts = string.split("=");
				if (parts[0].trim().equalsIgnoreCase("Log")) {
					this.logLevel = parts[1].trim().toUpperCase();
				}

				if (parts[0].trim().equalsIgnoreCase("RecoveryBinID")) {
					this.recoveryBinID = parts[1].trim();
				}
			}
		}

		this.logDebug("[findParameters]: Log: " + this.logLevel + " recoveryBinID: " + this.recoveryBinID);
		this.logDebug("[findParameters() time]: " + timerToString(this.getTimer(_start)));
	}

	public void logOut(String arg) {
		if (arg.equals("\n")) {
			System.out.println(arg);
		} else {
			System.out.println("[Delete2RecoveryBean [1.0.7]] [" + this.sourceObjectId.toString() + "] [" + arg + "]");
		}

	}

	public void logDebug(String arg) {
		if (this.logLevel.equals("D")) {
			if (arg.equals("\n")) {
				System.out.println(arg);
			} else {
				System.out.println(
						"[Delete2RecoveryBean [1.0.7]] [" + this.sourceObjectId.toString() + "] [" + arg + "]");
			}
		}

	}

	public void logErr(String arg) {
		System.err.println("[Delete2RecoveryBean [1.0.7]] [" + this.sourceObjectId.toString() + "] [" + arg + "]");
	}

	public static long resetTimer() {
		return System.currentTimeMillis();
	}

	public long getTimer(long start) {
		long now = System.currentTimeMillis();
		return now - start;
	}

	public static String timerToString(long nMillis) {
		long nHours = nMillis / 1000L / 60L / 60L;
		nMillis -= nHours * 1000L * 60L * 60L;
		long nMinutes = nMillis / 1000L / 60L;
		nMillis -= nMinutes * 1000L * 60L;
		long nSeconds = nMillis / 1000L;
		nMillis -= nSeconds * 1000L;
		StringBuffer time = new StringBuffer();
		if (nHours > 0L) {
			time.append(nHours + ":");
		}

		if (nHours > 0L && nMinutes < 10L) {
			time.append("0");
		}

		time.append(nMinutes + ":");
		if (nSeconds < 10L) {
			time.append("0");
		}

		time.append(nSeconds);
		time.append(".");
		if (nMillis < 100L) {
			time.append("0");
		}

		if (nMillis < 10L) {
			time.append("0");
		}

		time.append(nMillis);
		return time.toString();
	}
}