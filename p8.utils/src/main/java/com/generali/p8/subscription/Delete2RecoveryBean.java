package com.generali.p8.subscription;


import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.lang.model.type.NullType;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.VersionSeries;
import com.filenet.api.engine.EventActionHandler;
import com.filenet.api.events.ObjectChangeEvent;
import com.filenet.api.events.Subscription;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.util.CmRecoveryBin;
import com.filenet.api.util.CmRecoveryItem;
import com.filenet.api.util.Id;

public class Delete2RecoveryBean implements EventActionHandler {

	private static final String CLASS_NAME = Delete2RecoveryBean.class.getName();
	private static final PropertyFilter PF_USER_STRING = new PropertyFilter();
	private Id sourceObjectId;
	private ObjectStore objectStore;
	private  String logLevel="Y";
	private  String recoveryBinID="{70F73652-BCEC-4397-A8B4-8984E74931E5}";
	private static final String version = "1.0.7";
	private String prop = "DataCancellazioneLogica";


	static
	{
	PF_USER_STRING.addIncludeProperty(1, null, null, PropertyNames.USER_STRING, null);
	}
	
	public void onEvent(ObjectChangeEvent objectChangeEvent, Id subscriptionId)	throws EngineRuntimeException {
		long _start = resetTimer();   
		sourceObjectId = objectChangeEvent.get_SourceObjectId();	
    	objectStore = objectChangeEvent.getObjectStore();
		logOut(  " subscriptionId: " + subscriptionId.toString());

        try {
	 	    String userString = getUserString( objectChangeEvent, subscriptionId );
	 	    Document sourceDocument = (Document)Factory.Document.fetchInstance(objectStore, sourceObjectId, null);
			logDebug("[onEvent]:" +  " SourceDocument: " + sourceDocument.get_Id() +   "SourceDocument Owner: " + sourceDocument.get_Owner() +   "ObjectChangeEvent Owner: " + objectChangeEvent.get_Owner());
	 	    // EC spostato righe di modifica nella execute insieme al resto della business logic			
	 	    execute(objectStore, sourceDocument , userString);
			logDebug("[Running time totale]: "  + timerToString(getTimer(_start)));
        } catch (Exception e) {
	    	throw new  EngineRuntimeException(ExceptionCode.E_FAILED, "Errore" + e.getMessage());
		}
	
	}

	public void execute(ObjectStore objectStore, Document sourceDocument , String userString) throws Exception {
		    
			long _start = resetTimer();   
			logDebug("[execute() Start]" );
			
        	/* EC - inizio modifica Data Cancellazione Logica ===============================*/
			logDebug("[execute]:" + " SourceDocument ["+ sourceDocument.get_Id() +"]: Aggiornamento ["+ prop +"]-> nuovo Valore [ null ]" );
	 	    sourceDocument.getProperties().putObjectValue(prop, null);
	 	    sourceDocument.save(RefreshMode.NO_REFRESH);
			/* - fine modifica Data Cancellazione Logica ====================================*/

	 	    findParameters(userString, objectStore, sourceObjectId);
            PropertyFilter propertyFilter = new PropertyFilter(); 
            propertyFilter.addIncludeProperty(new FilterElement(null, null, null, "RecoveryItems , DisplayName", null)); 
            CmRecoveryBin cmRecoveryBin = Factory.CmRecoveryBin.fetchInstance(objectStore, new Id(recoveryBinID), propertyFilter); 
            VersionSeries versionSeries = sourceDocument.get_VersionSeries(); 
            CmRecoveryItem markedItem = versionSeries.markForDeletion(cmRecoveryBin, "CmRecoveryItem"); 
            markedItem.save(RefreshMode.NO_REFRESH);

    		logDebug("[execute() time]: "  + timerToString(getTimer(_start)));

	}

    


	private String getUserString(ObjectChangeEvent objectChangeEvent, Id subscriptionId) throws Exception {

		long _start = resetTimer();   
		logDebug("[getUserString() Start]" );
		Subscription subscription = Factory.Subscription.fetchInstance(objectChangeEvent.getObjectStore(),subscriptionId, PF_USER_STRING);
		logDebug("[getUserString() time]: "  + timerToString(getTimer(_start)));
		return subscription.get_UserString();
	}
		
	private void findParameters(String userString, ObjectStore objectStore, Id sourceObjectId) throws Exception {
		long _start = resetTimer();   
		logDebug("[findParameters() Start]" );


		logLevel="Y";


		
		if (userString.trim().length() > 0) {
			List<String> parameterist =  Arrays.asList(userString.split(";"));
			for (Iterator<String> iterator = parameterist.iterator(); iterator.hasNext();) {
				String string = iterator.next();
				String[] parts = string.split("=");
				if (parts[0].trim().equalsIgnoreCase("Log")) {
					logLevel = parts[1].trim().toUpperCase();
				}
				if (parts[0].trim().equalsIgnoreCase("RecoveryBinID")) {
					recoveryBinID = parts[1].trim();
				}

			}
		}

		logDebug("[findParameters]:" +  " Log: " + logLevel    + " recoveryBinID: " + recoveryBinID);
		logDebug("[findParameters() time]: "  + timerToString(getTimer(_start)));
	}

	public  void logOut(String arg)
	{
			if (arg.equals("\n")) {
				System.out.println(arg);
			} else {
				System.out.println("[Delete2RecoveryBean " + "[" + version + "]]"  + " [" + sourceObjectId.toString() + "] [" + arg + "]");		
			}
	}
	public  void logDebug(String arg)
	{
		if (logLevel.equals("D")) {
			if (arg.equals("\n")) {
				System.out.println(arg);
			} else {
				System.out.println("[Delete2RecoveryBean " + "[" + version + "]]"  + " [" + sourceObjectId.toString() + "] [" + arg + "]");		
			}
		}
	}

	public  void logErr(String arg)
	{
		System.err.println("[Delete2RecoveryBean " + "[" + version + "]]"  + " [" + sourceObjectId.toString() + "] [" + arg + "]");		
	}

	public static  long resetTimer() {
		return System.currentTimeMillis(); // now    
	}
	
	public  long getTimer(long start ) {
		long now = System.currentTimeMillis();
		return now - start;
	}

	/**
	 * Returns a formatted string showing the elaspsed time
	 * suince the instance was created.
	 * 
	.	 * @return  Formatted time string.
	 */
	public static String timerToString(long nMillis) {
	
		long nHours = nMillis / 1000 / 60 / 60;
		nMillis -= nHours * 1000 * 60 * 60;
	
		long nMinutes = nMillis / 1000 / 60;
		nMillis -= nMinutes * 1000 * 60;
	
		long nSeconds = nMillis / 1000;
		nMillis -= nSeconds * 1000;
	
		StringBuffer time = new StringBuffer();
		if (nHours > 0)
			time.append(nHours + ":");
		if (nHours > 0 && nMinutes < 10)
			time.append("0");
		time.append(nMinutes + ":");
		if (nSeconds < 10)
			time.append("0");
		time.append(nSeconds);
		time.append(".");
		if (nMillis < 100)
			time.append("0");
		if (nMillis < 10)
			time.append("0");
		time.append(nMillis);
	
		return time.toString();
	}	
}
