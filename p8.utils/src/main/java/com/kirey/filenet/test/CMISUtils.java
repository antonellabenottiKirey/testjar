package com.kirey.filenet.test;

import java.util.*;

import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.client.runtime.*;
import org.apache.chemistry.opencmis.commons.*;
import org.apache.chemistry.opencmis.commons.data.*;
import org.apache.chemistry.opencmis.commons.enums.*;



public class CMISUtils {
	
	//public static final String USER =  "ECMMIGSVIL@GGS";
	//public static final String USER =  "ECMMIGCERT@GGS";
	public static final String USER =  "ECMMIGPROD@GGS";
	//public static final String PASSWORD = "generali";
	public static final String PASSWORD = "GeneRa301421#";

	// SVIL
	//public static final String ATOMPUB_URL = "http://ecmmps01.generali.it:9062/fncmis/resources/Service";
	// CERT
	//public static final String ATOMPUB_URL = "http://ecmmpc01.generali.it:9081/fncmis/resources/Service";
	// PROD
	public static final String ATOMPUB_URL = "http://ecmmpp01.generali.it:9081/fncmis/resources/Service";
	public static final String BINDING_TYPE = BindingType.ATOMPUB.value();
	public static final String REPOSITORY_ID = "OBJMV04";
	
	public static final String KEY_PROP_NAME = "ProtocolloGED";
	
	
	Session session = null;
	
	public Session getSession() {
		return session;
	}

	public CMISUtils() {
		// default factory implementation
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Map<String, String> parameter = new HashMap<String, String>();

		// user credentials
		parameter.put(SessionParameter.USER, USER);
		parameter.put(SessionParameter.PASSWORD, PASSWORD);

		// connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, ATOMPUB_URL);
		parameter.put(SessionParameter.BINDING_TYPE, BINDING_TYPE);
		parameter.put(SessionParameter.REPOSITORY_ID, REPOSITORY_ID);
//		CmisBindingFactory factory = CmisBindingFactory.newInstance();
//		CmisBinding binding = factory.createCmisAtomPubBinding(parameter);
		// create session
		this.session = factory.createSession(parameter);
		System.out.println("Connected to: " + session.getRepositoryInfo().getName());
	}
	
	public static void main(String args[]){
        
		CMISUtils cmis = new CMISUtils();
		String query = "SELECT * FROM cmis:document where cmis:objectTypeId='SinistriEpa'";
		//query = "SELECT * FROM cmis:document where GSNumDanno='000040276'";
		//query = "SELECT ProtocolloGED, cmis:objectTypeId FROM cmis:document where cmis:lastModifiedBy='ECMMIGSVIL@GGS' and ProtocolloGED is not null";
		//query = "SELECT * FROM cmis:document where ProtocolloGED='GGLMS000000237900758'";
		query = "SELECT ProtocolloGED, DocumentTitle FROM cmis:document where cmis:objectTypeId in ('SinistriEPA') and DocumentTitle <>'TEST.pdf'";
		
		//cmis.printQuery(query);
		
		// update
		Map<String, Object> properties = new HashMap<String, Object>();
		//properties.put(property, newValue);
		String where = " protocolloged like 'GGLAG%' and cmis:lastModifiedBy='[CFS-IS Import Service]'";
		cmis.updateGGLAG("SinistriAttiGiudiziari", where, 10);
		
	}
	
	/**
	 * Print query results
	 * @param query
	 */
	public void printQuery(String query) {
		ItemIterable<QueryResult> results = this.getSession().query(query, false);
		int count = 0;
		for(QueryResult hit: results) {  
			//hit.getProperties().sort(null);
			count++;
		    for(PropertyData<?> property: hit.getProperties()) {

		        String queryName = property.getQueryName();
		        Object value = property.getFirstValue();

		        System.out.printf("[%s]\t%s\t", queryName, value);
		    }
		    System.out.println();
		}
		System.out.printf("Total records: %d(%d)\n", results.getTotalNumItems(), count);
	}
	
	/**
	 * Delete all documents in query
	 * @param objectTypeId
	 * @param where
	 */
	public void deleteQuery(String objectTypeId, String where) {
		ItemIterable<CmisObject> results = this.getSession().queryObjects(objectTypeId, where,
				false, this.getSession().getDefaultContext());
		int count = 0;
		for(CmisObject hit: results) {
			System.out.println("Deleting " + hit.getPropertyValue(KEY_PROP_NAME));
			hit.delete();
			count++;

		}
		System.out.printf("Total records: %d(%d)\n", results.getTotalNumItems(), count);
	}
	
	/**
	 * Update same properties in each document of the query
	 * @param objectTypeId
	 * @param where
	 */
	public void updateQuery(String objectTypeId, String where, Map<String, Object> props) {
		ItemIterable<CmisObject> results = this.getSession().queryObjects(objectTypeId, where,
				false, this.getSession().getDefaultContext());
		int count = 0;

		for(CmisObject hit: results) {
			System.out.println("Updating " + hit.getPropertyValue(KEY_PROP_NAME));
			hit.updateProperties(props);
			count++;

		}
		System.out.printf("Total records: %d(%d)\n", results.getTotalNumItems(), count);
	}
	
	/**
	 * Update ad-hoc to substitute GGLAG with GGLMS in ProtocolloGED
	 * @param objectTypeId
	 * @param where
	 */
	public void updateGGLAG(String objectTypeId, String where, long limit) {
		long start = System.currentTimeMillis();
		ItemIterable<CmisObject> results = this.getSession().queryObjects(objectTypeId, where,
				false, this.getSession().getDefaultContext());
		long end = System.currentTimeMillis();
		System.out.printf("Query time: %d ms\n", (end-start));
		int count = 0;
		start = System.currentTimeMillis();
		for(CmisObject hit: results) {
			//System.out.println("Updating " + hit.getPropertyValue(KEY_PROP_NAME));
			Map<String, Object> props = new HashMap<String, Object>();
			String prot = hit.getPropertyValue("ProtocolloGED");
			String newProt = prot.replaceFirst("GGLAG", "GGLMS");
			//System.out.printf("Replacing %s with %s\n", prot, newProt);
			props.put("ProtocolloGED", newProt);
			hit.updateProperties(props);
			if (count==limit)
				break;
			count++;
		}
		System.out.printf("Total records updated: %d\n", count);
		end = System.currentTimeMillis();
		System.out.printf("Update time: %d ms\n", (end-start));
	}
	





	
}
