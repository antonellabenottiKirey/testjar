package com.kirey.filenet.test;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.security.auth.Subject;

import com.filenet.api.admin.Choice;
import com.filenet.api.collection.CmRecoveryItemSet;
import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.DynamicReferentialContainmentRelationship;
import com.filenet.api.core.Factory;
import com.filenet.api.meta.ClassDescription;
import com.filenet.api.meta.PropertyDescription;
import com.filenet.api.core.Folder;
import com.filenet.api.core.IndependentObject;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.Properties;
import com.filenet.api.property.Property;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.CmRecoveryBin;
import com.filenet.api.util.CmRecoveryItem;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;
import org.apache.log4j.*;

public class Utils {
	private final static Logger LOG = Logger.getLogger(Utils.class);
	
	/* Criterio di selezione folder per WebCoass */
	private static final HashMap <String, String> coassMap = new HashMap<String, String>() {{ 
		put("003", "\\Coassicurazioni\\ALLIANZ_RAS");
		put("468", "\\Coassicurazioni\\RBM_SALUTE");
	}};
	private static final String COD_COMP_COASS = "CodCompagniaCoass";
	/**/
	
	
	Domain domain = null;
	ObjectStore os = null;
	private boolean simulationMode = true;
	public boolean isInSimulationMode() {
		return simulationMode;
	}
	
	public static final java.util.Properties props = new java.util.Properties();
	/* Caricamento Properties */
	static {
		InputStream stream = null;
		try {
			stream = Utils.class.getClassLoader().getResourceAsStream("p8utils.properties");
		    props.load(stream);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (stream!=null)
				try {
					stream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	
	public static final String NOSIM_PARAM = "NOSIM";
	public static final String URI = props.getProperty("uri");
	public static final String USERNAME = props.getProperty("user.name");
	public static final String PASSWORD = props.getProperty("user.password");
	public static final String OSNAME = props.getProperty("objectstore.name");
	public static final String JASSTANZA = props.getProperty("jas.stanza");
	public static final String DOMAIN = props.getProperty("domain.name");
	public static final String KEYPROP = props.getProperty("filenet.properties.key");
	public static final String NEWVALUE = props.getProperty("filenet.properties.newvalue");
	public static final String OLDVALUE = props.getProperty("filenet.properties.oldvalue");
	public static final String PATHDOWNLOAD = props.getProperty("path.download");
	public static final String BINID = props.getProperty("bin.id");
	public static final String YES = "Y";
	public static final String DATE_FORMAT="yyyyMMdd";
	
	public Utils() {
		Connection conn = getConnection(URI, USERNAME, PASSWORD, JASSTANZA);
		LOG.info("Connected to URI [" + URI + "]");
		this.domain = Factory.Domain.fetchInstance(conn, DOMAIN, null);
		LOG.info("Fetched domain [" + DOMAIN + "]");
		os = Factory.ObjectStore.fetchInstance(domain, OSNAME,null);
		LOG.info("Connected as [" +USERNAME+ "] to Object Store [" + OSNAME + "]");

	}
	
	public Connection getConnection(String uri, String username, String password, String jaasStanzaName) {
		Connection conn = Factory.Connection.getConnection(uri);

		Subject subject = UserContext.createSubject(conn, username, password, jaasStanzaName);
		UserContext uc = UserContext.get();
		subject.getPublicCredentials();
		uc.pushSubject(subject);
		return conn;
	}

	public static final void main(String[] args) {
		Utils ut = new Utils();
		String newFolder = "";
		String newClass = "";
		String filePath= "";
		
		if (args.length < 1) {
			LOG.error("Must provide a run option");
			System.exit(1);
		}
		// look for NOSIM param (simulation mode yes/no)
		if (Arrays.asList(args).contains(NOSIM_PARAM)) {
			ut.simulationMode = false;
			LOG.info("*** NO SIMULATION - changes will be effective ***");
			checkUserResponse();
			// remove NOSIM from params
			args = removeParam(args, NOSIM_PARAM);
		} else {
			ut.simulationMode = true;
			LOG.info("*** SIMULATION MODE ON - no harm will be done ***");
		}
		switch (args[0]) {
		case "REMOVE_DUPLICATES":	/* duplicate removal tool ====================*/
			ut.removeDuplicates();
			break;
		case "DELETE":	/* batch deletion tool ====================*/
			ut.delete();
			break;
		case "UPDATE":	/* update properties in pattern matching ================*/
			ut.updateProperty(KEYPROP, OLDVALUE, NEWVALUE);
			break;
		case "CHANGE_CLASS":	/* Change Class ==========================*/
			// arguments: class*
			if (args.length > 1) {
				newClass = args[1];
			} else {
				LOG.error("Must provide new class name!");
				System.exit(3);
			}
			ut.changeClass(newClass);
			break;
		case "CHANGE_FOLDER":	/* Change Class ==========================*/
			// arguments: folder*
			if (args.length > 1) {
				newFolder = args[1];
			} else {
				LOG.error("Must provide new folder name!");
				System.exit(4);
			}
			ut.changeFolder(newFolder);
			break;
		case "CREATE":	/* Create document ==========================*/
			// arguments: class*, path-to-file*, folder
			if (args.length > 1) {
				newClass = args[1];
				if (args.length > 2) {
					filePath = args[2];
				} else {
					LOG.error("Must provide path to content file!");
					System.exit(4);
				}
				if (args.length == 4) {
					newFolder = args[3];
				}
			} else {
				LOG.error("Must provide class name!");
				System.exit(4);
			}
			try {
				ut.createDocument(newClass, filePath, newFolder);
			} catch (Exception e) {
				LOG.error(e.getMessage());
				e.printStackTrace();
			} finally {
				ut.domain.clearPendingActions();
			}
			break;
		case "QUERY":	/* Query Semplice ========================*/
			ut.runQuery();
			break;
		case "QUERY_BIN":	/* Query sul recovery bin ========================*/
			ut.getBinContent();
			break;
		case "QUERY_BIN_ID":	/* Query sul recovery bin ========================*/
			ut.getBinItemById();
			break;
		case "DOWNLOAD":	/* Download documenti in query ========================*/
			try {
				ut.downloadContents();
			} catch (IOException ioe) {
				LOG.error(ioe.getMessage());
				ioe.printStackTrace();
			}
			break;
		case "CUSTOM":	/* Aggiornamento custom ========================*/
			ut.customExe();
			break;
		case "CUSTOM2":	/* Aggiornamento custom ========================*/
			ut.customExe2();
			break;
		default:
			LOG.error("Option [" + args[0] + "] unknown");
			System.exit(2);
		}
		//System.out.println(File.separator + "XX" + File.pathSeparator);
	}
	
	/**
	 * EC - to be used for ad-hoc tasks
	 */
	public void customExe() {
		String sql = props.getProperty("sql.custom");
		DocumentSet objs = getObjectSet(sql, 0);
		long count=0, updCount=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			if (count%1000==0) {
				LOG.info("" + count  + " records analyzed");
			}
			Document sourceDocument = (Document)Factory.Document.fetchInstance(os, doc.get_Id(), null);
			String prop = "DataCancellazioneLogica";
			LOG.info("[execute]:" + " SourceDocument ["+ sourceDocument.get_Id() + ":" + sourceDocument.getClassName() +"]: Aggiornamento ["+ prop +"] da [" +
					sourceDocument.getProperties().getStringValue(prop) + "] a [ null ]" );
			
			if (!this.isInSimulationMode()) {
				sourceDocument.getProperties().putObjectValue(prop, null);
				sourceDocument.save(RefreshMode.NO_REFRESH);
				updCount++;
			}
		}
		LOG.info("Total objects updated: " + updCount + "/" + count);
	}
	
	/**
	 * Parse file name and update date fields
	 * @throws ParseException 
	 */
	public void customExe2() {
		String sql = props.getProperty("sql.query");
		IndependentObjectSet objs = getObjectSet(sql, 0);
		long count=0, updCount=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		List<String> unparseable = new ArrayList<String>();
		while (it.hasNext()) {
			Document io = it.next();
			count++;
			Properties attrs = io.getProperties();
			//Iterator<Property> ip = attrs.iterator();
			String logKey = io.getProperties().get("ProtocolloGED").getStringValue();
			String fileName =  io.getProperties().get("DocumentTitle").getStringValue();
			LOG.info("["+logKey+"] DocumentTitle: " + fileName);
			String[] parsed = fileName.split("Da | a | - | al ");
			String start = parsed[1].trim();
			String end = parsed[2].trim();
			SimpleDateFormat df = new SimpleDateFormat("d MMMM yyyy", Locale.ITALIAN);
			Date startDate, endDate;
			try {
				startDate = df.parse(start);
				// gestione eccezioni su end date
				switch (end) {
				case "25 Feb18":
					endDate = df.parse("25 Febbraio 2018");
					break;
				case "INFORMAZIONI SUGLI OICR.pdf":
					end = null;
					endDate = null;
					break;
				default:
					endDate = df.parse(end);						
				}
				// check if date is valid
				Date firstValidDate = df.parse("1 Gennaio 1753");
				if (startDate != null  && startDate.compareTo(firstValidDate)<0) {
					LOG.warn("["+logKey+"] Data " + startDate + " is invalid - adding 2000...");
					Calendar cal = Calendar.getInstance();
				    cal.setTime(startDate);
				    cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+2000);
				    startDate = cal.getTime();
				}
				if (endDate != null && endDate.compareTo(firstValidDate)<0) {
					LOG.warn("["+logKey+"] Data " + endDate + " is invalid - adding 2000...");
					Calendar cal = Calendar.getInstance();
				    cal.setTime(endDate);
				    cal.set(Calendar.YEAR, cal.get(Calendar.YEAR)+2000);
				    endDate = cal.getTime();
				}
			} catch (ParseException p) {
				LOG.error("["+logKey+"] Unable to parse date: " + p.getMessage());
				LOG.error("Aborting...");
				unparseable.add(fileName + ": " + p.getMessage());
				continue;
			}
			Property pStart = io.getProperties().get("SVGDataInizioValiditaModello");
			Property pEnd = io.getProperties().get("SVGDataFineValiditaModello");
			LOG.info("["+ logKey +"] Replacing from " + pStart.getDateTimeValue() + " to " + startDate + "\n\t and from " + pEnd.getDateTimeValue() + " to " + endDate);
			if (!this.isInSimulationMode()) {
				io.getProperties().putObjectValue("SVGDataInizioValiditaModello", startDate);
				io.getProperties().putObjectValue("SVGDataFineValiditaModello", endDate);
				io.save(RefreshMode.NO_REFRESH);
				updCount++;
			}
		}
		LOG.info("Total rows changed: " + updCount);
		if (!unparseable.isEmpty())
			LOG.warn("Unparseable (" + unparseable.size()  +"): \n\t" + unparseable.toString().replaceAll(",", "\n\t"));
	}
	
	/**
	 * Run query specified in config property sql.query and show results
	 */
	public void runQuery() {
		String sql = props.getProperty("sql.query");
		IndependentObjectSet objs = getObjectSet(sql, 0);
		long count=0, updCount=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			IndependentObject io = it.next();
			count++;
			Properties attrs = io.getProperties();
			Iterator<Property> ip = attrs.iterator();
			if (count==1) {
				while (ip.hasNext()) {
					Property p = ip.next();
					// show headers
					System.out.print(p.getPropertyName() + "\t");
				}
				ip = attrs.iterator();		// reset
			}
			System.out.println();
			while (ip.hasNext()) {
				Property p = ip.next();
				// show attributes
				System.out.print(p.getObjectValue()+"\t");
			}
			
		}
		System.out.println();
		LOG.info("Total rows returned: " + count);
	}
	
//	protected String propConverter(Property p) {
//		switch (p.get)
//	}
//	
	/**
	 * update a property value
	 * @param prop - Property to be updated
	 * @param from - pattern to be replaced (can be empty or null)
	 * @param to - replacement string; if no "from" is provided, it will be used as brand new value. If "null", will be used <null> value
	 */
	public void updateProperty(String prop, String from, String to) {
		String sql = props.getProperty("sql.replace");
		DocumentSet objs = getObjectSet(sql, 0);
		long count=0, updCount=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			if (count%1000==0) {
				LOG.info("" + count  + " records analyzed");
			}
			String prot = doc.getProperties().get(prop).getStringValue();
			String newValue = (from==null || from.isEmpty()) ? to : prot.replaceFirst(from, to);
			LOG.info("Replacing from " + prot + " to " + newValue);
			if (!this.isInSimulationMode()) {
				if (newValue.equals("null"))
					newValue = null;
				doc.getProperties().putValue(prop, newValue);
				doc.save(RefreshMode.NO_REFRESH);
				updCount++;
			}
			
		}
		LOG.info("Total objects updated: " + updCount + "/" + count);
	}
	
	/**
	 * Find and remove duplicates in the provided query
	 */
	public void removeDuplicates() {
		String sql = props.getProperty("sql.duplicates");
		DocumentSet objs = getObjectSet(sql, 0);
		long dupCount = 0, count=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		String lastProt = "";
		String lastTitle = "";
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			if (count%1000==0) {
				LOG.info("" + count  + " records analyzed");
			}
			String prot = doc.getProperties().get("ProtocolloGED").getStringValue();
			String title = doc.getProperties().get("DocumentTitle").getStringValue();
			//LOG.debug("[" + lastProt+lastTitle + "] ?= [" + prot+title +"]");
			if ((lastProt+lastTitle).equals(prot+title)) {
				dupCount++;
				Double size = doc.getProperties().get("ContentSize").getFloat64Value();
				LOG.warn("Duplicate found: ProtocolloGED[" + prot
						+ "], DocumentTitle["+title+"] - " + size);
				if (!this.isInSimulationMode()) {
					// rimozione documento immediata
					doc.delete();
					doc.save(RefreshMode.REFRESH);
					LOG.warn("Documento eliminato");
				}
			}
			lastProt = prot;
			lastTitle = title;
		}
		LOG.info("Total duplicates found: " + dupCount + "/" + count);
	}
	
	/**
	 * delete items in the provided query
	 */
	public void delete() {
		String sql = props.getProperty("sql.query");
		DocumentSet objs = getObjectSet(sql, 0);
		long dupCount = 0, count=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			if (count%1000==0) {
				LOG.info("" + count  + " records analyzed");
			}
			String prot = doc.getProperties().get("ProtocolloGED").getStringValue();
			String title = doc.getProperties().get("DocumentTitle").getStringValue();
			LOG.warn("I'm going to delete: ProtocolloGED[" + prot
					+ "], DocumentTitle["+title+"]");
			if (!this.isInSimulationMode()) {
				// rimozione documento immediata
				doc.delete();
				doc.save(RefreshMode.REFRESH);
				LOG.warn("Document deleted");
			}
		}
		LOG.info("Total documents deleted: " + count);
	}
	
	
	/**
	 * Change class of documents in provided query to newClass
	 * @param newClass - name of the class to change documents to
	 */
	public void changeClass(String newClass) {
		// check new class is valid
		ClassDescription objClassDesc = Factory.ClassDescription.fetchInstance(os, newClass, null);
		if (objClassDesc==null) {
			LOG.error("Class [" + newClass + "] not found on server!");
			System.exit(4);
		}
		// use standard query
		String sql = props.getProperty("sql.query");
		DocumentSet objs = getObjectSet(sql, 0);
		long chgCount=0,count=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			if (count%1000==0) {
				LOG.info("" + count  + " records analyzed");
			}
			String currentClass = doc.getClassName();
			
			if (!currentClass.equals(newClass)) {
				String prot = doc.getProperties().get(KEYPROP).getStringValue();
				LOG.info("Changing [" + prot +"] from current class ["
						+ currentClass + "] to new class [" + objClassDesc.get_DisplayName() + "]...");
				if (!this.isInSimulationMode()) {
					// change class
					doc.changeClass(newClass);
					doc.save(RefreshMode.NO_REFRESH);
					chgCount++;
					LOG.warn("Documento aggiornato");
				}
			}

		}
		LOG.info("Total documents changed: " + chgCount + "/" + count);
	}

	/**
	 * Move documents in provided query to newFolder
	 * @param newFolder - name of the Folder to move documents in
	 */
	public void changeFolder(String newFolder) {
		// check new class is valid
		LOG.info("Folder to: " + newFolder);
		Folder destinationFolder = Factory.Folder.fetchInstance(os, newFolder, null);
		if (destinationFolder==null) {
			LOG.error("Folder [" + newFolder + "] not found on server!");
			System.exit(5);
		}
		// use standard query
		String sql = props.getProperty("sql.query");
		DocumentSet objs = getObjectSet(sql, 0);
		long chgCount=0,count=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			if (count%1000==0) {
				LOG.info("" + count  + " records analyzed");
			}
			FolderSet fs = doc.get_FoldersFiledIn();
			try {
				String currentFolderName="\\";
	
				/* this folder assignment criteria are valid ONLY for WebCoass */
				//String destinationFolderName = coassMap.get(doc.getProperties().getStringValue(COD_COMP_COASS));	
				String destinationFolderName = newFolder;
				//logOut("destinationFolderName : " + destinationFolderName);
				destinationFolder = Factory.Folder.fetchInstance(os, destinationFolderName, null); 
				//LOG.info("Compagnia coass: " + destinationFolderName);
				/* fine criterio WebCoass */
				String prot = doc.getProperties().get(KEYPROP).getStringValue();

				// unfile from current folder, if not unfiled
				if (fs != null && !fs.isEmpty()) {
					Folder currentFolder = (Folder) doc.get_FoldersFiledIn().iterator().next();
					currentFolderName = currentFolder.get_FolderName();
					LOG.info("Unfiling [" + prot +"] from folder [" + currentFolderName + "] ...");
					if (!this.isInSimulationMode()) {
						DynamicReferentialContainmentRelationship drcrFileOut = (DynamicReferentialContainmentRelationship)currentFolder.unfile(doc);
						drcrFileOut.save(RefreshMode.REFRESH);
					}
				}
				// file to new folder
				LOG.info("Filing [" + prot +"] into new folder [" + destinationFolder.get_FolderName() + "]...");
				if (!this.isInSimulationMode()) {
					DynamicReferentialContainmentRelationship drcrFileIn = (DynamicReferentialContainmentRelationship)destinationFolder.file(doc, AutoUniqueName.NOT_AUTO_UNIQUE, doc.get_Name(), DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
			    	drcrFileIn.save(RefreshMode.REFRESH);
			    	doc.refresh();
				    LOG.info("Document moved.");
				    chgCount++;
				}
		    } catch (Exception e) {
				if (e instanceof EngineRuntimeException) {
					EngineRuntimeException erx = (EngineRuntimeException) e;
					if (erx.getExceptionCode().equals(ExceptionCode.E_NOT_UNIQUE) ) {
				    	LOG.error("Exception: " + e.getMessage() + "\n" +
				    			"The name " + "\"" + doc.get_Name() + "\"" + " is already used.\n" + 
				    			"Please file the document with a unique name.");
				    } else
				    	  LOG.error("EngineRuntimeException: " + e.getMessage());
				} else
					LOG.error("Error message: " + e.getMessage());
		    }
			// SUBSCRIPTION CODE
		}
		LOG.info("Total documents changed: " + chgCount + "/" + count);
	}
	
	/**
	 * Create a new document with provided property map and specified content (to be loaded locally)
	 * @param targetClass - the document class where the document has to be archived
	 * @param filePath - local file path to retrieve di binary content
	 * @param targetFolder - (optional) the folder where to file the new document
	 * @throws Exception
	 */
	public void createDocument(String targetClass, String filePath, String targetFolder) throws Exception {
		int newCount = 0;
		LOG.info("Loading document [" + filePath + "] + into class [" + targetClass + "]...");
		// Create a document instance.
		Document doc = Factory.Document.createInstance(os, targetClass);

		// Set document properties. ------------------
		// load provided properties
		java.util.Properties metadata = loadProperties(props.getProperty("filenet.properties.map"));
		ClassDescription objClassDesc = Factory.ClassDescription.fetchInstance(os, targetClass, null);
		// load a map with class-specific properties and types (do it once and for all)
		Map<String,TypeID> pTypes = new HashMap<String,TypeID>();
		Iterator it = objClassDesc.get_PropertyDescriptions().iterator(); 
		while (it.hasNext()) {
			PropertyDescription pDesc = (PropertyDescription)it.next();
			//LOG.debug("Adding " + pDesc.get_SymbolicName() + ":" +  pDesc.get_DataType());
			pTypes.put(pDesc.get_SymbolicName(), pDesc.get_DataType());
		}
		// match provided properties with class properties and set by type
		for (Map.Entry<Object,Object> prop : metadata.entrySet()) {
			String key = (String)prop.getKey();
			Object value = null;
			TypeID pType = pTypes.get(key);
			if (pType == null) {
				throw new Exception("Property [" + key + "] not found for class [" + targetClass + "]");
			}
			//TODO use different methods based on property type (necessary for dates)
			switch (pType.getValue()) {
			case TypeID.LONG_AS_INT:
				LOG.debug("Type of [" + key +  "]:" + pType.getValue());
				value = Integer.parseInt((String) prop.getValue());
				break;
			case TypeID.DATE_AS_INT:
				LOG.debug("Type of [" + key +  "]:" + pType.getValue());
				Date dateValue=new SimpleDateFormat(DATE_FORMAT).parse((String) prop.getValue()); 
				value = dateValue;
				break;
			default:
				value = prop.getValue();
			}
			LOG.debug("Writing property [" + key + "] with value: " + value);
			doc.getProperties().putObjectValue(key, value);
		}
		
		// Set Content
		if (!this.isInSimulationMode()) {
			doc.save(RefreshMode.NO_REFRESH );
			this.addContent(doc, filePath);
			// Check in the document.
			doc.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			LOG.info("Document created.");
			newCount++;
		}
		
		// check if target folder is valid
		LOG.info("Folder to: " + targetFolder);
		if (targetFolder != null && !targetFolder.isEmpty()) {
			Folder destinationFolder = Factory.Folder.fetchInstance(os, targetFolder, null);
			if (destinationFolder==null) {
				throw new Exception("Folder [" + targetFolder + "] not found on server!");
			}
			// file to new folder
			LOG.info("Filing [" + doc +"] into new folder [" + destinationFolder.get_FolderName() + "]...");
			if (!this.isInSimulationMode()) {
				DynamicReferentialContainmentRelationship drcrFileIn = (DynamicReferentialContainmentRelationship)destinationFolder.file(doc, AutoUniqueName.NOT_AUTO_UNIQUE, doc.get_Name(), DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
		    	drcrFileIn.save(RefreshMode.REFRESH);
		    	doc.refresh();
			    LOG.info("Document filed.");
			    //chgCount++;
			}
		} else {
			LOG.warn("No folder specified, document will be left unfiled.");
		}
		LOG.info("Total documents created: " + newCount);
	}
	
	public void downloadContents() throws IOException {
		// use standard query
		String sql = props.getProperty("sql.download");
		DocumentSet objs = getObjectSet(sql, 0);
		long count=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		List<String> errors = new ArrayList<String>();
		while (it.hasNext()) {
			Document doc = it.next();
			count++;
			// build file name
			String fileName = PATHDOWNLOAD + File.separator + doc.getProperties().getStringValue("DocumentTitle") + "."
					+ doc.getProperties().getStringValue("EstensioneDocOrgn");
			LOG.info("Will write content to [" + fileName + "]");
			FileOutputStream file = null;
			if (!this.isInSimulationMode()) {
				file = new FileOutputStream(fileName);
			}
			try {
				Iterator<ContentTransfer> contents = doc.get_ContentElements().iterator();
				while (contents.hasNext() ) {
			    	 ContentTransfer ct = contents.next();
			    	 // Print generic data on content element
			    	 LOG.info("Element " + ct.get_RetrievalName() + "(type: " + ct.get_ContentType() + " / size: " + ct.get_ContentSize() + ")");
			    	 if (!this.isInSimulationMode()) {
				    	 InputStream stream = ct.accessContentStream();
				    	 byte[] buffer = new byte[4096000];
				    	 int bytesRead = 0;
				    	 try {
				    		 // transfer bytes
					    	 while ((bytesRead = stream.read(buffer)) != -1) {
					    		 file.write(buffer,0,bytesRead);
					         }
				    	 } finally {
				    		 if (stream != null)
				    			 stream.close();
				    	 }
			    	 }
			     }
			} catch (Exception e) {
				LOG.error(e.getMessage() + " - Skipping document " + fileName + "...");
				errors.add(fileName);
				LOG.warn("Deleting " + fileName + "...");
				File f = new File(fileName);
				if (f.exists()) f.delete();
			} finally {
				if (file != null)
					file.close();
			}
		}
		LOG.info("Download process complete");
		if (!errors.isEmpty()) {
			LOG.warn("Skipped " + errors.size() + " document(s) of " + count + ": "+errors);
		}
	}
	
	public void getBinContent() {

		PropertyFilter pf = new PropertyFilter();
		
		// Get the recovery bin from the Id passed to the method.
		pf.addIncludeProperty(new FilterElement(null, null, null, "RecoveryItems", null));
		pf.addIncludeProperty(new FilterElement(null, null, null, "ProtocolloGED", null));
		CmRecoveryBin myBin = com.filenet.api.core.Factory.CmRecoveryBin.fetchInstance(os,
				new Id(BINID), pf);
		
		// Get the recovery items contained by the recovery bin.
		CmRecoveryItemSet items = myBin.get_RecoveryItems();
		Iterator iter = items.iterator();
		int num = 0;
		while (iter.hasNext())
		{
		   CmRecoveryItem item = (CmRecoveryItem) iter.next();
//		   // Perform operation as specified by flag passed to the method.
//		   if (actionFlag==ACTION_RECOVER) item.recover();
//		   else if (actionFlag==ACTION_PURGE) item.delete();
//		   item.save(RefreshMode.REFRESH);
		   Properties attrs = item.getProperties();
		   System.out.println(attrs.getStringValue("OriginalName"));
		   num++;
		}
		LOG.info("Total number of items in Recovery Bin: " + num);
	}
	
	public void getBinItemById() {
		String sql = props.getProperty("sql.query");
		PropertyFilter pf = new PropertyFilter();
        pf.addIncludeProperty(new FilterElement(null, null, null, "ProtocolloGED", 1));
		IndependentObjectSet objs = getObjectSet(sql, 0, pf);
		long count=0, updCount=0;
		LOG.info("Iterating objects...");
		Iterator<Document> it = objs.iterator();
		while (it.hasNext()) {
			IndependentObject io = it.next();
			count++;
			CmRecoveryItem r = (CmRecoveryItem) io.getProperties().getObjectValue("This");
			r.recover();
			LOG.info("oggetto recuperato");
		}
	}
	
	/**
	 * Get user response to proceed with updates
	 */
	private static void checkUserResponse() {
		System.out.println("Proceed? (Y/n)");
		byte[] response= new byte[1];
		try {
			System.in.read(response);
			if (!(new String(response).equals(YES))) {
				LOG.fatal("No authorization to proceed -- Quitting...");
				System.exit(1);
			} else {
				LOG.info("* Authorized * -- proceeding...");;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	private DocumentSet getObjectSet(String sql, int maxHits) {
		SearchSQL sqlObject = new SearchSQL();
		if (maxHits>0)
			sqlObject.setMaxRecords(maxHits);
		sqlObject.setQueryString(sql);
		SearchScope searchScope = new SearchScope(os);
		LOG.info("Starting query: " + sql);
		DocumentSet objs = (DocumentSet)searchScope.fetchObjects(sqlObject, null, null, true);
		LOG.info("End query.");
		return objs;
	}
	
	private DocumentSet getObjectSet(String sql, int maxHits, PropertyFilter pf) {
		SearchSQL sqlObject = new SearchSQL();
		if (maxHits>0)
			sqlObject.setMaxRecords(maxHits);
		sqlObject.setQueryString(sql);
		SearchScope searchScope = new SearchScope(os);
		LOG.info("Starting query: " + sql);
		DocumentSet objs = (DocumentSet)searchScope.fetchObjects(sqlObject, null, pf, true);
		LOG.info("End query.");
		return objs;
	}
	
	
	/* DI CECIO -------------------------------------------------------------------------*/
	private  String  codiceMacroCategoriaDocumentoId="{72CA9EDC-2E7E-48DA-84CF-8330F9D94F09";
    private  String  codiceTipoDocumentoID="{7FE946CE-BF08-4988-BC54-CFCD9B9F6C1D}";
    private  String  reload="N";
    private static HashMap<Integer, Integer> hashMapMacroCategorieTipoDocumento;
    /**
     * Load specific choice list Macrocategorie
     * @param objectStore
     */
    private void loadMacrocategorie(ObjectStore objectStore) {
        long _start = System.currentTimeMillis();  
        //logDebug("[loadMacrocategorie() Start]" );
        if (reload.equalsIgnoreCase("N")) {
               if (hashMapMacroCategorieTipoDocumento != null) {
                      if (!hashMapMacroCategorieTipoDocumento.isEmpty()) {
                             //logDebug("[loadMacrocategorie() hashMapMacroCategorieTipoDocumento gi� inizializzata time]: "  + timerToString(getTimer(_start)));
                             return;
                      }
               }
        }
        HashMap<String , Integer> hashMapMacroCategorie = new HashMap<String , Integer>();
        hashMapMacroCategorieTipoDocumento = new HashMap<Integer, Integer>();
        Id choiceListId = new Id(codiceMacroCategoriaDocumentoId);
        com.filenet.api.admin.ChoiceList choiceListObj = Factory.ChoiceList.fetchInstance(objectStore, choiceListId, null);
        com.filenet.api.collection.ChoiceList choiceList = choiceListObj.get_ChoiceValues();
        Iterator<Choice> iterator = choiceList.iterator();
               while (iterator.hasNext()) {
                      Choice choice =  iterator.next();
                      hashMapMacroCategorie.put( choice.get_DisplayName().trim(), choice.get_ChoiceIntegerValue());
        }
        choiceListId = new Id(codiceTipoDocumentoID);
        choiceListObj = Factory.ChoiceList.fetchInstance(objectStore, choiceListId, null);
        choiceList = choiceListObj.get_ChoiceValues();
        String macroCategoria="";
        Integer codiceMacroCategoria;
        iterator = choiceList.iterator();
        while (iterator.hasNext()) {
        	Choice choice =  iterator.next();
	        macroCategoria = choice.get_DisplayName().trim();
	        codiceMacroCategoria =  hashMapMacroCategorie.get(macroCategoria);
	        LOG.debug("[Codice]: " + codiceMacroCategoria +  " [Descrizione]: " + choice.get_DisplayName() +  " [Type]: "  +  choice.get_ChoiceType());
	        com.filenet.api.collection.ChoiceList choiceListChoice = choice.get_ChoiceValues();
	        Iterator<Choice> iteratorChoice = choiceListChoice.iterator();
	        while (iteratorChoice.hasNext()) {
	               Choice choiceChoice =  iteratorChoice.next();
	                hashMapMacroCategorieTipoDocumento.put(choiceChoice.get_ChoiceIntegerValue(), codiceMacroCategoria);
	        }
        }

        //logDebug("[loadMacrocategorie() time]: "  + timerToString(getTimer(_start)));
              
  }
    /**
     * Load a map from a String formatted like "key1:value1,key2:value2,key3:value3..."
     * @param sProp
     * @return
     */
    private static java.util.Properties loadProperties(String sProp) throws Exception {
    	java.util.Properties map = new java.util.Properties();
    	try {
	    	for (String pair : sProp.split(",")) {
	    		String[] pairArray = pair.split(":");
	    		map.put(pairArray[0].trim(), pairArray[1].trim());
	    	}
    	} catch (ArrayIndexOutOfBoundsException e) {
    		throw new Exception("Metadata string malformed in property file: " + sProp);
    	}
    	return map;
    }
    
    /**
     * Add a local file as document content
     * @param doc
     * @param filePath
     */
    private void addContent(Document doc, String filePath) throws Exception {
    	// Check out the Document object and save it.
    	//doc.checkout(com.filenet.api.constants.ReservationType.EXCLUSIVE, null, doc.getClassName(), doc.getProperties());
    	doc.save(RefreshMode.REFRESH);
    	// Get the Reservation object from the Document object.
    	Document reservation = (Document) doc.get_Reservation();
    	// Get the local file
    	File localFile = new File(filePath);
    	// Add content to the Reservation object.
    	try {
    	    // First, add a ContentTransfer object.
    	    ContentTransfer ctObject = Factory.ContentTransfer.createInstance();
    	    FileInputStream fileIS = new FileInputStream(localFile.getAbsolutePath());
    	    ContentElementList contentList = Factory.ContentElement.createList();
    	    ctObject.setCaptureSource(fileIS);
    	    String contentType = Files.probeContentType(localFile.toPath());
    	    LOG.debug("Content type: " + contentType);
    	    ctObject.set_ContentType(contentType);
			ctObject.set_RetrievalName(doc.get_Name());
    	    // Add ContentTransfer object to list.
    	    contentList.add(ctObject);
    	    reservation.set_ContentElements(contentList);
    	    reservation.save(RefreshMode.REFRESH);
    	} catch (Exception e) {
    		throw e;
    	}
    	
    	// Check in Reservation object as major version.
    	reservation.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
    	reservation.save(RefreshMode.REFRESH);
    }
    
    /**
     * Cut off an element from an array of strings (namely the main argument array)
     * @param args
     * @param param
     * @return
     */
    private static String[] removeParam(String[] args, String param) {
    	String[] newArgs;
    	List<String> pList = (List<String>) Arrays.asList(args);
    	List<String> pList2 = new ArrayList<String>();
    	pList2.addAll(pList);
    	pList2.remove(param);
    	newArgs = pList2.toArray(new String[pList2.size()]);
    	return newArgs;
    }
}
