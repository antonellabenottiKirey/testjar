package com.kirey.filenet;

import java.net.SocketException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import javax.security.auth.Subject;

import org.apache.log4j.Logger;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.UpdatingBatch;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.meta.ClassDescription;
import com.filenet.api.meta.PropertyDescription;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.util.UserContext;
import com.google.common.collect.Iterators;
import com.kirey.filenet.bean.MetadataBean;
import com.kirey.filenet.bean.ProcessResult;
import com.kirey.filenet.bean.WorkingDocumentBean;
import com.kirey.filenet.dao.WorkingDocumentsDAO;

public class ProcessorThread implements Callable<ProcessResult> {

	private static final Logger LOG = Logger.getLogger(ProcessorThread.class);

	private final String threadName;
	private ObjectStore os = null;
	private ProcessResult processResult;
	private Map<String, Integer> propertyTypes = new HashMap<String, Integer>();

	ProcessorThread(int threadNumber) {
		this.threadName = "thread" + threadNumber;
	}

	public ProcessResult call() {
		processResult = new ProcessResult();
		processResult.setThreadName(threadName);
		try {
			LOG.info("Thread " + threadName + " started");
			connectToObjectStore();
			List<WorkingDocumentBean> wdList = WorkingDocumentsDAO.getInstance().selectAndLockRecordsForWork(ProjectProperties.DIM_LOTTO);
			LOG.info(threadName + " loaded " + wdList.size() + " records");
			processResult.setInitialRecords(wdList.size());
			changeClass(wdList);
			processResult.setThreadStatus(ProcessResult.THREAD_STATUS_OK);
		} catch (Exception e) {
			LOG.error(getErrorMessage(e));
			processResult.setThreadStatus(ProcessResult.THREAD_STATUS_ERROR);
		}
		LOG.info("Thread " + threadName + " ended");
		return processResult;
	}

	public String getThreadName() {
		return threadName;
	}

	private DocumentSet getDocumentSet(String sql, int maxHits) throws SocketException {
		SearchSQL sqlObject = new SearchSQL();
		if (maxHits > 0) {
			sqlObject.setMaxRecords(maxHits);
		}
		sqlObject.setQueryString(sql);
		SearchScope searchScope = new SearchScope(os);
		LOG.debug("Starting query: " + sql);
		DocumentSet objs = (DocumentSet) searchScope.fetchObjects(sqlObject, null, null, true);
		LOG.debug("End query.");
		return objs;
	}

	private void connectToObjectStore() {
		Connection conn = getConnection(ProjectProperties.P8_URL, ProjectProperties.P8_USERNAME, ProjectProperties.P8_PASSWORD,
				ProjectProperties.P8_STANZA_NAME);
		LOG.info("Connected to URI [" + ProjectProperties.P8_URL + "]");
		Domain domain = Factory.Domain.fetchInstance(conn, ProjectProperties.P8_DOMAIN_NAME, null);
		LOG.info("Fetched domain [" + ProjectProperties.P8_DOMAIN_NAME + "]");
		this.os = Factory.ObjectStore.fetchInstance(domain, ProjectProperties.P8_OBJECT_STORE_NAME, null);
		LOG.info("Connected as [" + ProjectProperties.P8_USERNAME + "] to Object Store [" + ProjectProperties.P8_OBJECT_STORE_NAME + "]");
	}

	private Connection getConnection(String uri, String username, String password, String jaasStanzaName) {
		Connection conn = Factory.Connection.getConnection(uri);

		Subject subject = UserContext.createSubject(conn, username, password, jaasStanzaName);
		UserContext uc = UserContext.get();
		subject.getPublicCredentials();
		uc.pushSubject(subject);
		return conn;
	}

	private void changeClass(List<WorkingDocumentBean> wdList) throws SocketException {
		LOG.debug("Entering changeClass");
		UpdatingBatch updBatch = UpdatingBatch.createUpdatingBatchInstance(os.get_Domain(), RefreshMode.NO_REFRESH);
		List<WorkingDocumentBean> processedOkDocs = new ArrayList<WorkingDocumentBean>();
		int processedCounter = 0;
		for (int k = 0; k < wdList.size(); k++) {
			WorkingDocumentBean wdBean = wdList.get(k);
			LOG.debug(threadName + " processing row " + processedCounter + "/" + wdList.size());
			try {
				String protocolloGED = wdBean.getProtocolloGed();
				// Checks if the target class is present in the ObjectStore
				ClassDescription targetClassDesc = Factory.ClassDescription.fetchInstance(os, wdBean.getTargetClass(), null);
				if (targetClassDesc != null) {

					String query = "SELECT id, ProtocolloGED FROM Document WHERE ProtocolloGED = '" + protocolloGED + "'";
					DocumentSet ds = getDocumentSet(query, 10);

					int documentsFound = Iterators.size(ds.iterator());
					if (documentsFound == 1) {
						Iterator<Document> itDS = ds.iterator();
						while (itDS.hasNext()) {
							processedCounter++;
							Document document = itDS.next();
							String currentClass = document.getClassName();
							if (!currentClass.equals(wdBean.getTargetClass())) {
								LOG.debug("Changing id " + document.get_Id() + " from current class [" + currentClass + "] to new class ["
										+ wdBean.getTargetClass() + "]...");
								document.changeClass(wdBean.getTargetClass());
							} else {
								// No change class needed - Just change the metadata
								LOG.warn("Document was already of class " + wdBean.getTargetClass());
							}
							for (MetadataBean md : wdBean.getMetaData()) {
								LOG.debug("Setting " + md.getMetadataName() + "=" + md.getMetadataValue() + " for id " + document.get_Id());
								document.getProperties().putObjectValue(md.getMetadataName(), getMetadataValueAsObject(wdBean.getTargetClass(), md));
							}

							if (ProjectProperties.IS_BULK_MODE) {
								updBatch.add(document, null);
								processedOkDocs.add(wdBean);
							} else {
								LOG.debug("Update in SINGLE mode");
								if (!ProjectProperties.IS_SIMULATION_MODE) {
									document.save(RefreshMode.REFRESH);
								}
								WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_PROCESSED_OK);
							}
							processResult.addOK();
						}
					} else if (documentsFound == 0) {
						// No docs found for the ProtocolloGED
						String errorMessage = "No documents found for ProtocolloGED=" + protocolloGED;
						LOG.error(errorMessage);
						WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_FATAL_ERROR, errorMessage);
						processResult.addFatalerror();
					} else {
						String errorMessage = "More than one document found (" + documentsFound + ") for ProtocolloGED=" + protocolloGED;
						LOG.error(errorMessage);
						WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_RECOVERABLE_ERROR, errorMessage);
						processResult.addRecoverableError();
					}
				} else {
					String errorMessage = "Class [" + wdBean.getTargetClass() + "] not found on server!";
					LOG.error(errorMessage);
					WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_FATAL_ERROR, errorMessage);
					processResult.addFatalerror();
				}
			} catch (EngineRuntimeException ere) {
				// TODO check if it matches with recoverable/fatal
				LOG.error(getErrorMessage(ere));
				if (ere.getExceptionCode().isFatal()) {
					WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_FATAL_ERROR, getErrorMessage(ere));
					processResult.addFatalerror();
				} else {
					WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_RECOVERABLE_ERROR, getErrorMessage(ere));
					processResult.addRecoverableError();
				}
			} catch (Exception e) {
				LOG.error(getErrorMessage(e));
				WorkingDocumentsDAO.getInstance().updateStatus(wdBean.getID(), WorkingDocumentBean.STATUS_RECOVERABLE_ERROR, getErrorMessage(e));
				processResult.addRecoverableError();
			}
			if (ProjectProperties.IS_BULK_MODE && !processedOkDocs.isEmpty()
					&& (processedOkDocs.size() % ProjectProperties.NUM_ROW_UPDATE == 0 || (k + 1) == wdList.size())) {
				// If in BULK_MODE, every NUM_ROW_UPDATE, update Filenet and DB
				LOG.debug("Update in BULK mode after: " + processedCounter + " rows");
				if (!ProjectProperties.IS_SIMULATION_MODE) {
					if (updBatch.hasPendingExecute()) {
						updBatch.updateBatch();
					}
				}
				WorkingDocumentsDAO.getInstance().updateStatus(processedOkDocs.stream().map(WorkingDocumentBean::getID).collect(Collectors.toList()),
						WorkingDocumentBean.STATUS_PROCESSED_OK, "");
				processedOkDocs.clear();

			}
		}
	}

	// Returns the metadata as Object, converting it from String (db format)
	private Object getMetadataValueAsObject(String className, MetadataBean md) throws ParseException {
		Object value = null;
		switch (getDataType(className, md.getMetadataName())) {
		case TypeID.LONG_AS_INT:
			value = Integer.parseInt((String) md.getMetadataValue());
			break;
		case TypeID.DATE_AS_INT:
			String DATE_FORMAT = "yyyyMMdd";
			Date dateValue = new SimpleDateFormat(DATE_FORMAT).parse((String) md.getMetadataValue());
			value = dateValue;
			break;
		default:
			value = md.getMetadataValue();
		}
		return value;
	}

	// Caches the property object types
	private int getDataType(String className, String propertyName) {
		String elementKey = getPropertyTypeKey(className, propertyName);
		if (!propertyTypes.containsKey(elementKey)) {
			ClassDescription objClassDesc = Factory.ClassDescription.fetchInstance(os, className, null);
			Iterator<PropertyDescription> it = objClassDesc.get_PropertyDescriptions().iterator();
			while (it.hasNext()) {
				PropertyDescription propertyDescription = it.next();
				LOG.debug("Adding " + propertyDescription.get_SymbolicName() + ":" + propertyDescription.get_DataType() + " for " + className);
				propertyTypes.put(getPropertyTypeKey(className, propertyDescription.get_SymbolicName()), propertyDescription.get_DataType().getValue());
			}
		}
		if (!propertyTypes.containsKey(elementKey)) {
			LOG.error(className + " does not contain a property " + propertyName);
		}
		return propertyTypes.get(elementKey);
	}

	// Returns the key for the property in the internal map - to have a single
	// dimension map
	private String getPropertyTypeKey(String className, String propertyName) {
		return new StringBuffer(className).append(".").append(propertyName).toString();
	}

	private String getErrorMessage(Exception e) {
		return e.getMessage() != null ? e.getMessage() : e.toString();
	}

}
