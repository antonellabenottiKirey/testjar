package com.kirey.filenet.bean;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class ProcessResult {

	public static final String THREAD_STATUS_OK = "OK";
	public static final String THREAD_STATUS_ERROR = "ERROR";

	private String threadStatus = "";
	private int initialRecords = 0;
	private int OKCount = 0;
	private int recoverableCount = 0;
	private int fatalCount = 0;
	private String threadName;
	private ZonedDateTime startTime = ZonedDateTime.now();
	private ZonedDateTime endTime;

	public String getThreadStatus() {
		return threadStatus;
	}

	public void setThreadStatus(String threadStatus) {
		this.threadStatus = threadStatus;
		this.endTime = ZonedDateTime.now();
	}

	public int getInitialRecords() {
		return initialRecords;
	}

	public void setInitialRecords(int initialRecords) {
		this.initialRecords = initialRecords;
	}

	public int getOKCount() {
		return OKCount;
	}

	public void addOK() {
		OKCount++;
	}

	public int getRecoverableCount() {
		return recoverableCount;
	}

	public void addRecoverableError() {
		recoverableCount++;
	}

	public int getFatalCount() {
		return fatalCount;
	}

	public void addFatalerror() {
		fatalCount++;
	}

	public String getThreadName() {
		return threadName;
	}

	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}

	public long getSecondsDuration() {
		return ChronoUnit.SECONDS.between(startTime, endTime);
	}

}
