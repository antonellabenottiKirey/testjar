package com.kirey.filenet.bean;

import java.sql.Date;

public class UploadFilesBean {

	private int id;
	private String pathFile = "";
	private long rows;
	private long rowsLoaded;
	private Date dateInserted;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPathFile() {
		return pathFile;
	}

	public void setPathFile(String pathFile) {
		this.pathFile = pathFile;
	}

	public long getRows() {
		return rows;
	}

	public void setRows(long rows) {
		this.rows = rows;
	}

	public long getRowsLoaded() {
		return rowsLoaded;
	}

	public void setRowsLoaded(long rowsLoaded) {
		this.rowsLoaded = rowsLoaded;
	}

	public Date getDateInserted() {
		return dateInserted;
	}

	public void setDateInserted(Date dateInserted) {
		this.dateInserted = dateInserted;
	}
}
