package com.kirey.filenet.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WorkingDocumentBean {

	private int ID;
	private String protocolloGED = "";
	private String sourceClass = "";
	private String targetClass = "";
	private long idUploadFile = 0;
	private Date dateInserted;
	private Date lastUpdate;
	private String status = "";
	private int retries = 0;
	private String errorDescription = "";
	private List<MetadataBean> metaData = new ArrayList<MetadataBean>();

	public static final String STATUS_NEW = "NW";
	public static final String STATUS_PROCESSING = "PR";
	public static final String STATUS_PROCESSED_OK = "OK";
	public static final String STATUS_RECOVERABLE_ERROR = "RE";
	public static final String STATUS_FATAL_ERROR = "FE";

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getProtocolloGed() {
		return protocolloGED;
	}

	public void setProtocolloGed(String protocolloGed) {
		this.protocolloGED = protocolloGed;
	}

	public String getSourceClass() {
		return sourceClass;
	}

	public void setSourceClass(String sourceClass) {
		this.sourceClass = sourceClass;
	}

	public String getTargetClass() {
		return targetClass;
	}

	public void setTargetClass(String targetClass) {
		this.targetClass = targetClass;
	}

	public long getIdUploadFile() {
		return idUploadFile;
	}

	public void setIdUploadFile(long idUploadFile) {
		this.idUploadFile = idUploadFile;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getRetries() {
		return retries;
	}

	public void setRetries(int retries) {
		this.retries = retries;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	public List<MetadataBean> getMetaData() {
		return metaData;
	}

	public void setMetaData(List<MetadataBean> metaData) {
		this.metaData = metaData;
	}

	public Date getDateInserted() {
		return dateInserted;
	}

	public void setDateInserted(Date dateInserted) {
		this.dateInserted = dateInserted;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
