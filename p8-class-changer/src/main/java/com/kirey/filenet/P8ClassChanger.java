package com.kirey.filenet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.kirey.filenet.bean.MetadataBean;
import com.kirey.filenet.bean.ProcessResult;
import com.kirey.filenet.bean.UploadFilesBean;
import com.kirey.filenet.bean.WorkingDocumentBean;
import com.kirey.filenet.dao.MetadataFieldsDAO;
import com.kirey.filenet.dao.UploadFilesDAO;
import com.kirey.filenet.dao.WorkingDocumentsDAO;
import com.kirey.filenet.exception.InsertException;

public class P8ClassChanger {

	private static final Logger LOG = Logger.getLogger(P8ClassChanger.class);
	private static final String HEADER_PROTOCOLLO_GED = "ProtocolloGED";
	public static File folderInput = new File(ProjectProperties.PATH_FOLDER_INPUT);
	public static File folderOutput = new File(ProjectProperties.PATH_FOLDER_PROCESSED);

	P8ClassChanger() {
		LOG.info("Starting in " + (ProjectProperties.IS_BULK_MODE ? "BULK" : "SINGLE") + " mode");
		LOG.info("Simulation mode: " + (ProjectProperties.IS_SIMULATION_MODE ? "Enabled" : "Disabled"));
		//WorkingDocumentsDAO.getInstance().resetAllProcessingRecords(); // TODO
		// REMOVE!!!
		listFilesForFolder(folderInput, folderOutput);
		startThreads();

		// Check if there are documents in "processing" status
		List<WorkingDocumentBean> processingDocs = WorkingDocumentsDAO.getInstance().selectRecordByStatus(WorkingDocumentBean.STATUS_PROCESSING);
		if (processingDocs.size() > 0) {
			LOG.error("There are records in PROCESSING status in the internal DB:");
			for (WorkingDocumentBean wdBean : processingDocs) {
				LOG.error("Internal ID: " + wdBean.getID());
			}
		} else {
			LOG.info("No records in PROCESSING status were found");
		}

		printStatistics();
	}

	private void printStatistics() {
		LOG.info("Final DB situation:");
		LOG.info("Rows in status NEW: " + WorkingDocumentsDAO.getInstance().selectCountByStatus(WorkingDocumentBean.STATUS_NEW));
		LOG.info("Rows in status PROCESSING: " + WorkingDocumentsDAO.getInstance().selectCountByStatus(WorkingDocumentBean.STATUS_PROCESSING));
		LOG.info("Rows in status OK: " + WorkingDocumentsDAO.getInstance().selectCountByStatus(WorkingDocumentBean.STATUS_PROCESSED_OK));
		LOG.info("Rows in status RECOVERABLE ERROR: " + WorkingDocumentsDAO.getInstance().selectCountByStatus(WorkingDocumentBean.STATUS_RECOVERABLE_ERROR));
		LOG.info("Rows in status FATAL ERROR: " + WorkingDocumentsDAO.getInstance().selectCountByStatus(WorkingDocumentBean.STATUS_FATAL_ERROR));
	}

	// Returns true if all the rows were correctly loaded
	private boolean loadInputFile(File file) {
		boolean loadOK = true;
		try {
			String targetFromFile = getTargetClassFromFile(file);
			String sourceFromFile = getSourceClassFromFile(file);
			LOG.info("source class = " + sourceFromFile + " - " + "target class: " + targetFromFile);
			CSVParser csvParser = null;
			if (!targetFromFile.equals("") && (!sourceFromFile.equals(""))) {
				csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().withDelimiter(ProjectProperties.DELIMITER)
						.parse(Files.newBufferedReader(Paths.get(file.getPath())));
			} else {
				LOG.error("\n" + "length of file name not correct! Follow the following pattern:MIG_Source_Target_yyymmdd.csv");
			}
			UploadFilesDAO uploadFilesDAO = UploadFilesDAO.getInstance();
			List<CSVRecord> records = csvParser.getRecords();
			int idUploadFile = uploadFilesDAO.getIdByPathFile(file.getPath());
			if (idUploadFile == 0) {
				UploadFilesBean uploadFilesBean = new UploadFilesBean();
				uploadFilesBean.setPathFile(file.getPath());
				uploadFilesBean.setRows(records.size());
				uploadFilesBean.setRowsLoaded(0);
				uploadFilesBean.setDateInserted(new Date(System.currentTimeMillis()));
				idUploadFile = uploadFilesDAO.insert(uploadFilesBean);
			}
			List<String> headers = csvParser.getHeaderNames();
			int processed = 0;
			int written = 0;
			for (CSVRecord record : records) {
				WorkingDocumentBean wdBean = new WorkingDocumentBean();
				try {
					WorkingDocumentsDAO wdDAO = WorkingDocumentsDAO.getInstance();
					MetadataFieldsDAO mdDAO = MetadataFieldsDAO.getInstance();
					int id = wdDAO.getNextID();
					wdBean.setID(id);
					wdBean.setProtocolloGed(record.get(HEADER_PROTOCOLLO_GED));
					wdBean.setSourceClass(sourceFromFile);
					wdBean.setTargetClass(targetFromFile);
					wdBean.setIdUploadFile(idUploadFile);
					wdBean.setDateInserted(new Date(System.currentTimeMillis()));
					wdBean.setLastUpdate(wdBean.getDateInserted());
					wdBean.setStatus(WorkingDocumentBean.STATUS_NEW);
					wdBean.setRetries(0);
					wdBean.setErrorDescription("");
					if (!wdDAO.existRecord(wdBean.getIdUploadFile(), wdBean.getProtocolloGed())) {
						wdDAO.insert(wdBean);
						for (String header : headers) {
							if (!HEADER_PROTOCOLLO_GED.equals(header)) {
								MetadataBean md = new MetadataBean();
								md.setID(id);
								md.setMetadataName(header);
								md.setMetadataValue(record.get(header));
								mdDAO.insert(md);
							}
						}
						// Moved after metadata insert: if there's an error, no record will be inserted
						written++;
					} else {
						LOG.warn("Row already present in the database: " + wdBean.getProtocolloGed());
					}
				} catch (IllegalArgumentException iae) {
					LOG.error(getErrorMessage(iae) + " - " + wdBean.getProtocolloGed());
					loadOK = false;
				}

				if (++processed % ProjectProperties.NUM_ROW_UPDATE == 0 || processed >= records.size()) {
					LOG.info("Processed " + processed + " lines from file " + file.getName() + ", written " + written);
				}
			}
			LOG.info("End loading file: " + file);
		} catch (IOException ioe) {
			LOG.error(getErrorMessage(ioe));
			loadOK = false;
		} catch (InsertException e) {
			LOG.error("Error inserting row");
			loadOK = false;
		}
		return loadOK;
	}

	private void listFilesForFolder(final File folderIn, final File folderOut) {
		try {
			List<File> returnFilesIn = new ArrayList<File>();
			for (final File fileIn : folderIn.listFiles()) {
				if (fileIn.isFile()) {
					returnFilesIn.add(fileIn);
				}
			}
			for (int i = 0; i < returnFilesIn.size(); i++) {
				try {
					if (folderOut.exists()) {
						File fileIn = new File(folderOut.getPath() + "/" + returnFilesIn.get(i).getName());
						if (!fileIn.exists()) {
							boolean loadOK = loadInputFile(returnFilesIn.get(i));
							if (loadOK) {
								LOG.debug("Start moving file: " + returnFilesIn.get(i).getName());
								FileUtils.moveFileToDirectory(returnFilesIn.get(i), folderOut, true);
								LOG.debug("End - file moved: " + returnFilesIn.get(i).getName() + " - " + "file moved to folder: " + folderOut.getPath());
							}
						} else {
							LOG.error("file: " + fileIn.getName() + "already present in the folder: " + folderOut.getPath());
						}
					} else {
						LOG.error("the folder: " + folderOut.getName() + " does not exist");
					}
				} catch (Exception e) {
					LOG.error(getErrorMessage(e));
				}
			}
		} catch (Exception e) {
			LOG.error(getErrorMessage(e));
		}
	}

	private void startThreads() {
		ZonedDateTime startTime = ZonedDateTime.now();
		ExecutorService executor = Executors.newFixedThreadPool(ProjectProperties.NUM_THREAD_CONTEMP);
		try {
			List<ProcessorThread> taskList = new ArrayList<>();
			for (int i = 0; i < ProjectProperties.NUM_THREAD_TOTAL; i++) {
				ProcessorThread thread = new ProcessorThread(i);
				taskList.add(thread);
			}
			List<Future<ProcessResult>> resultList = executor.invokeAll(taskList);
			for (Future<ProcessResult> future : resultList) {
				try {
					ProcessResult processResult = future.get();
					System.out.println("Results for thread " + processResult.getThreadName() + " - Status:" + processResult.getThreadStatus() + " Initial:"
							+ processResult.getInitialRecords() + " OK:" + processResult.getOKCount() + " RE:" + processResult.getRecoverableCount() + " FE:"
							+ processResult.getFatalCount() + " - Duration: " + processResult.getSecondsDuration() + " seconds");
				} catch (ExecutionException ee) {
					LOG.error(getErrorMessage(ee));
				} catch (InterruptedException ie) {
					LOG.error(getErrorMessage(ie));
				} catch (Exception e) {
					LOG.error(getErrorMessage(e));
				}
			}
		} catch (InterruptedException ie) {
			LOG.error(getErrorMessage(ie));
		} catch (Exception e) {
			LOG.error(getErrorMessage(e));
		} finally {
			LOG.info("Threads shutdown");
			LOG.info("Total time: " + ChronoUnit.SECONDS.between(startTime, ZonedDateTime.now()));
			executor.shutdown();
		}
	}

	public String getTargetClassFromFile(File file) {
		String fileName = file.getName();
		String[] splitName = fileName.split("_");
		String target = splitName[2];
		return target;
	}

	public String getSourceClassFromFile(File file) {
		String fileName = file.getName();
		String[] splitName = fileName.split("_");
		String source = splitName[1];
		return source;
	}

	public static void main(String[] args) throws IOException {
		P8ClassChanger p8ClassChanger = new P8ClassChanger();
	}

	private String getErrorMessage(Exception e) {
		return e.getMessage() != null ? e.getMessage() : e.toString();
	}

}
