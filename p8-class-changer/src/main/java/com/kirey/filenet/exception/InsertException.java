package com.kirey.filenet.exception;

public class InsertException extends Exception {
	public InsertException(String errorMessage) {
		super(errorMessage);
	}

}
