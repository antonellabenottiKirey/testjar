package com.kirey.filenet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ProjectProperties {

	private static final Logger LOG = Logger.getLogger(ProjectProperties.class);
	public static final Properties props = new Properties();
	/* Caricamento Properties */
	static {
		try {
			File jarPath = new File(ProjectProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			String propertiesPath = jarPath.getParentFile().getAbsolutePath();
			props.load(new FileInputStream(propertiesPath + "/p8classChanger.properties"));
			LOG.info("End of loading ProjectProperties from " + propertiesPath);
		} catch (IOException e) {
			try {
				String pathFile = (new File(System.getProperty("user.dir"))).getParentFile().getAbsolutePath();
				props.load(new FileInputStream(pathFile + "/p8classChanger.properties"));
				LOG.info("End of loading ProjectProperties from " + pathFile);
			} catch (IOException e1) {
				LOG.error(e.getMessage());
				e1.printStackTrace();
			}

		}
	}

	/* Properties */
	public static final String P8_OBJECT_STORE_NAME = props.getProperty("P8_OBJECT_STORE_NAME");
	public static final String P8_USERNAME = props.getProperty("P8_USERNAME");
	public static final String P8_PASSWORD = props.getProperty("P8_PASSWORD");
	public static final String P8_URL = props.getProperty("P8_URL");
	public static final String P8_SOURCE_CLASS = props.getProperty("P8_SOURCE_CLASS");
	public static final String P8_TARGET_CLASS = props.getProperty("P8_TARGET_CLASS");
	public static final String P8_DOMAIN_NAME = props.getProperty("P8_DOMAIN_NAME");
	public static final String P8_STANZA_NAME = props.getProperty("P8_STANZA_NAME");
	public static final char DELIMITER = props.getProperty("DELIMITER").charAt(0);
	public static final int NUM_THREAD_TOTAL = Integer.parseInt(props.getProperty("NUM_THREAD_TOTAL"));
	public static final int NUM_THREAD_CONTEMP = Integer.parseInt(props.getProperty("NUM_THREAD_CONTEMP"));
	public static final int DIM_LOTTO = Integer.parseInt(props.getProperty("DIM_LOTTO"));
	public static final String DB_URL_CON = props.getProperty("DB_URL_CON");
	public static final String DB_NOME = props.getProperty("DB_NOME");
	public static final String DB_PATH = props.getProperty("DB_PATH");
	public static final String DB_USER = props.getProperty("DB_USER");
	public static final String DB_PASSWORD = props.getProperty("DB_PASSWORD");
	public static final String PATH_FOLDER_INPUT = props.getProperty("PATH_FOLDER_INPUT");
	public static final String PATH_FOLDER_PROCESSED = props.getProperty("PATH_FOLDER_PROCESSED");
	public static final boolean IS_BULK_MODE = "Y".equalsIgnoreCase(props.getProperty("BULK_MODE")) ? true : false; // Better to go in SINGLE mode
	public static final boolean IS_SIMULATION_MODE = "N".equalsIgnoreCase(props.getProperty("SIMULATION_MODE")) ? false : true; // Better to go in SIMULATION
																																// mode
	public static final int NUM_ROW_UPDATE = Integer.parseInt(props.getProperty("NUM_ROW_UPDATE"));

}
