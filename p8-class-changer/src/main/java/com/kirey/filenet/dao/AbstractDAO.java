package com.kirey.filenet.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.kirey.filenet.ProjectProperties;

public abstract class AbstractDAO {

	private static final Logger LOG = Logger.getLogger(AbstractDAO.class);
	protected static Connection conn = null;

	/* connessione DB */
	protected Connection connect() {
		String url = ProjectProperties.DB_URL_CON + ":" + ProjectProperties.DB_PATH + ProjectProperties.DB_NOME;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, ProjectProperties.DB_USER, ProjectProperties.DB_PASSWORD);
		} catch (SQLException e) {
			LOG.error(e.getMessage() + " " + e.getCause());
		}
		return conn;
	}

}
