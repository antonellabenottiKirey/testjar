package com.kirey.filenet.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.kirey.filenet.bean.MetadataBean;
import com.kirey.filenet.exception.InsertException;

public class MetadataFieldsDAO extends AbstractDAO {

	private static MetadataFieldsDAO mdDAO;
	private static final Logger LOG = Logger.getLogger(MetadataFieldsDAO.class);
	private static final String SELECT_SQL = "SELECT WORKING_DOCUMENTS_ID, METADATA_NAME, METADATA_VALUE FROM METADATA_FIELDS WHERE WORKING_DOCUMENTS_ID = ?";
	private static final String INSERT_SQL = "INSERT INTO METADATA_FIELDS (WORKING_DOCUMENTS_ID, METADATA_NAME, METADATA_VALUE) VALUES (?, ?, ?)";

	private MetadataFieldsDAO() throws SQLException {
		if (conn == null) {
			conn = this.connect();
		}
	}

	public static synchronized MetadataFieldsDAO getInstance() {
		try {
			if (mdDAO == null) {
				mdDAO = new MetadataFieldsDAO();
			}
		} catch (SQLException se) {
			LOG.error(se.getMessage());
		}
		return mdDAO;
	}

	public List<MetadataBean> selectByID(int id) {
		PreparedStatement pstmtSelect = null;
		ResultSet rsSelect = null;
		ArrayList<MetadataBean> returnValue = new ArrayList<MetadataBean>();
		try {
			pstmtSelect = conn.prepareStatement(SELECT_SQL);
			pstmtSelect.setInt(1, id);
			LOG.debug("INIZIO SELECT " + SELECT_SQL);
			rsSelect = pstmtSelect.executeQuery();
			while (rsSelect.next()) {
				MetadataBean bean = new MetadataBean();
				bean.setID(rsSelect.getInt("WORKING_DOCUMENTS_ID"));
				bean.setMetadataName(rsSelect.getString("METADATA_NAME"));
				bean.setMetadataValue(rsSelect.getString("METADATA_VALUE"));
				returnValue.add(bean);
			}
			LOG.debug("FINE SELECT, numero di righe trovate:  " + returnValue.size());
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmtSelect.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return returnValue;
	}

	public void insert(MetadataBean bean) throws InsertException {
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(INSERT_SQL);
			pstmt.setInt(1, bean.getID());
			pstmt.setString(2, bean.getMetadataName());
			pstmt.setString(3, bean.getMetadataValue());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			LOG.error(e.getMessage() + " " + e.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

}
