package com.kirey.filenet.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;

import com.kirey.filenet.bean.UploadFilesBean;
import com.kirey.filenet.exception.InsertException;

public class UploadFilesDAO extends AbstractDAO {

	private static UploadFilesDAO mdDAO;
	private static final Logger LOG = Logger.getLogger(UploadFilesDAO.class);
	private static final String SELECT_SQL = "SELECT * FROM UPLOAD_FILES WHERE ID = ?";
	private static final String INSERT_SQL = "INSERT INTO UPLOAD_FILES (PATH_FILE, ROWS, ROWS_LOADED, DATE_INSERTED) VALUES (?, ?, ?, ?)";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private UploadFilesDAO() throws SQLException {
		if (conn == null) {
			conn = this.connect();
		}
	}

	public static synchronized UploadFilesDAO getInstance() {
		try {
			if (mdDAO == null) {
				mdDAO = new UploadFilesDAO();
			}
		} catch (SQLException se) {
			LOG.error(se.getMessage());
		}
		return mdDAO;
	}

	public UploadFilesBean getById(int id) throws InsertException {
		PreparedStatement pstmt = null;
		ResultSet rsSelect = null;
		UploadFilesBean bean = null;
		try {
			pstmt = conn.prepareStatement(SELECT_SQL);
			pstmt.setInt(1, id);
			rsSelect = pstmt.executeQuery();
			while (rsSelect.next()) {
				bean = new UploadFilesBean();
				populateBeanByResultSet(rsSelect, bean);

			}
		} catch (SQLException e) {

		} finally {
			try {
				pstmt.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return bean;
	}

	public int getIdByPathFile(String pathFile) throws InsertException {
		PreparedStatement pstmt = null;
		ResultSet rsSelect = null;
		int retValue = 0;
		try {
			String sqlSelect = "SELECT ID FROM UPLOAD_FILES WHERE PATH_FILE=? ";
			pstmt = conn.prepareStatement(sqlSelect);
			pstmt.setString(1, pathFile);
			rsSelect = pstmt.executeQuery();
			while (rsSelect.next()) {
				retValue = rsSelect.getInt("ID");
			}
		} catch (SQLException e) {
			LOG.error(e.getMessage());
		} finally {
			try {
				pstmt.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return retValue;
	}

	public int insert(UploadFilesBean bean) throws InsertException {
		PreparedStatement pstmt = null;
		int retValue = 0;
		try {
			pstmt = conn.prepareStatement(INSERT_SQL);
			populatePreparedStatement(pstmt, bean);
			pstmt.executeUpdate();
			retValue = this.getIdByPathFile(bean.getPathFile());
		} catch (SQLException e) {
			LOG.error("Error while inserting " + e.getMessage());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return retValue;
	}

	private void populatePreparedStatement(PreparedStatement pstmt, UploadFilesBean bean) throws SQLException {
		pstmt.setString(1, bean.getPathFile());
		pstmt.setLong(2, bean.getRows());
		pstmt.setLong(3, bean.getRowsLoaded());
		pstmt.setString(4, sdf.format(bean.getDateInserted()));
	}

	public void populateBeanByResultSet(ResultSet rs, UploadFilesBean bean) throws SQLException {
		bean.setId(rs.getInt("ID"));
		bean.setPathFile(rs.getString("PATH_FILE"));
		bean.setRows(rs.getInt("ROWS"));
		bean.setRowsLoaded(rs.getInt("ROWS_LOADED"));
		bean.setDateInserted(rs.getDate("DATE_INSERTED"));
	}

}
