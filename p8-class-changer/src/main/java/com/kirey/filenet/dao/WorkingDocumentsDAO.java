package com.kirey.filenet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.kirey.filenet.ProjectProperties;
import com.kirey.filenet.bean.WorkingDocumentBean;
import com.kirey.filenet.exception.InsertException;

public class WorkingDocumentsDAO extends AbstractDAO {
	private static WorkingDocumentsDAO wdDAO;
	private static Connection conn = null;
	private static final Logger LOG = Logger.getLogger(WorkingDocumentsDAO.class);
	private static final String INSERT_SQL = "INSERT INTO WORKING_DOCUMENTS (ID, PROTOCOLLO_GED, SOURCE_CLASS, TARGET_CLASS, ID_UPLOAD_FILE, DATE_INSERTED, LAST_UPDATE, STATUS, RETRIES) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final Object lockVariable = new Object();

	private WorkingDocumentsDAO() throws SQLException {
		if (conn == null) {
			conn = this.connect();
		}
	}

	public static synchronized WorkingDocumentsDAO getInstance() {
		try {
			if (wdDAO == null) {
				wdDAO = new WorkingDocumentsDAO();
			}
		} catch (SQLException se) {
			LOG.error(se.getMessage());
		}
		return wdDAO;
	}

	public List<WorkingDocumentBean> selectAndLockRecordsForWork(int limit) {
		LOG.debug("Waiting for selectAndLockRecordsForWork lock");
		synchronized (lockVariable) {
			LOG.debug("End waiting for selectAndLockRecordsForWork lock");
			String selectSql = "SELECT * FROM WORKING_DOCUMENTS WHERE STATUS IN ('" + WorkingDocumentBean.STATUS_NEW + "','"
					+ WorkingDocumentBean.STATUS_RECOVERABLE_ERROR + "') LIMIT ?";
			PreparedStatement pstmtSelect = null;
			ResultSet rsSelect = null;
			ArrayList<WorkingDocumentBean> returnValue = new ArrayList<WorkingDocumentBean>();
			try {
				pstmtSelect = conn.prepareStatement(selectSql);
				pstmtSelect.setInt(1, limit);
				LOG.debug("START SELECT " + selectSql);
				rsSelect = pstmtSelect.executeQuery();
				LOG.debug("END SELECT, number of lines found: :  " + returnValue.size());

				while (rsSelect.next()) {
					WorkingDocumentBean bean = new WorkingDocumentBean();
					populateBeanByResultSet(rsSelect, bean);
					returnValue.add(bean);
				}

				// Updates all the rows together
				if (!returnValue.isEmpty()) {
					this.updateForLock(returnValue.stream().map(WorkingDocumentBean::getID).collect(Collectors.toList()));
				}
				LOG.debug("END updateForLock");
			} catch (SQLException throwables) {
				LOG.error(throwables.getMessage() + " " + throwables.getCause());
			} finally {
				try {
					pstmtSelect.close();
					rsSelect.close();
				} catch (SQLException sqle) {
					LOG.error(sqle.getMessage() + " " + sqle.getCause());
				}
			}
			LOG.debug("End of selectAndLockRecordsForWork synchronized block");
			return returnValue;
		}
	}

	// Bulk update for lock
	private void updateForLock(List<Integer> ids) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < ids.size(); i++) {
			builder.append("?,");
		}
		String sql = "UPDATE WORKING_DOCUMENTS SET STATUS='" + WorkingDocumentBean.STATUS_PROCESSING + "', RETRIES = RETRIES+1, LAST_UPDATE= ? WHERE ID IN ("
				+ builder.deleteCharAt(builder.length() - 1).toString() + ")";

		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sdf.format(new Date(System.currentTimeMillis())));
			int index = 2;
			for (Integer value : ids) {
				pstmt.setInt(index++, value);
			}
			pstmt.executeUpdate();
			LOG.debug("end updateForLock");

		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	public void insert(WorkingDocumentBean bean) throws InsertException {
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(INSERT_SQL);
			populatePreparedStatement(pstmt, bean);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			if (e.getErrorCode() == 19) {
				LOG.error(e.getMessage() + " " + e.getCause());
			} else {
				throw new InsertException("Errore in inserimento record ");
			}
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	public void bulkInsert(List<WorkingDocumentBean> lstBean) {
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(INSERT_SQL);
			for (int i = 0; i < lstBean.size(); i++) {
				WorkingDocumentBean bean = lstBean.get(i);
				populatePreparedStatement(pstmt, bean);
				pstmt.addBatch();
				if ((i + 1) % ProjectProperties.NUM_ROW_UPDATE == 0 || (i + 1) == lstBean.size()) {
					pstmt.executeBatch();
					LOG.debug("Records inserted: " + (i + 1));
					pstmt.clearBatch();
				}
			}
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	public void updateStatus(int id, String status) {
		updateStatus(id, status, null);
	}

	public void updateStatus(int id, String status, String errorDescription) {
		String sql = "UPDATE WORKING_DOCUMENTS SET STATUS=?, ERROR_DESCRIPTION=?, LAST_UPDATE= ?WHERE ID=?";
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, status);
			pstmt.setString(2, errorDescription != null ? errorDescription : "");
			pstmt.setString(3, sdf.format(new Date(System.currentTimeMillis())));
			pstmt.setInt(4, id);
			pstmt.executeUpdate();
			LOG.debug("end updateStatus");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	public void updateStatus(List<Integer> ids, String status, String errorDescription) {
		String sql = "UPDATE WORKING_DOCUMENTS SET STATUS=?, ERROR_DESCRIPTION=? ,LAST_UPDATE= ? WHERE ID=? ";
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			for (int i = 0; i < ids.size(); i++) {
				pstmt.setString(1, status);
				pstmt.setString(2, errorDescription != null ? errorDescription : "");
				pstmt.setString(3, sdf.format(new Date(System.currentTimeMillis())));
				pstmt.setInt(4, ids.get(i));
				pstmt.addBatch();
				if ((i + 1) % ProjectProperties.NUM_ROW_UPDATE == 0 || (i + 1) == ids.size()) {
					pstmt.executeBatch();
					LOG.debug("Records updated (STATUS=" + status + ", ERROR_DESCRIPTION=" + errorDescription + "): " + (i + 1));
					pstmt.clearBatch();
				}
			}
			LOG.debug("end updateStatus");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	private void updateRetries(int id) {
		String sql = "UPDATE WORKING_DOCUMENTS SET RETRIES = RETRIES+1, LAST_UPDATE= ? WHERE ID=?";
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, sdf.format(new Date(System.currentTimeMillis())));
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
			LOG.debug("fine updateRetries");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	public void populatePreparedStatement(PreparedStatement pstmt, WorkingDocumentBean bean) throws SQLException {
		pstmt.setInt(1, bean.getID());
		pstmt.setString(2, bean.getProtocolloGed());
		pstmt.setString(3, bean.getSourceClass());
		pstmt.setString(4, bean.getTargetClass());
		pstmt.setLong(5, bean.getIdUploadFile());
		pstmt.setString(6, sdf.format(bean.getDateInserted()));
		pstmt.setString(7, sdf.format(bean.getLastUpdate()));
		pstmt.setString(8, bean.getStatus());
		pstmt.setInt(9, bean.getRetries());
	}

	public void populateBeanByResultSet(ResultSet rs, WorkingDocumentBean bean) throws SQLException {
		bean.setID(rs.getInt("ID"));
		bean.setProtocolloGed(rs.getString("PROTOCOLLO_GED"));
		bean.setSourceClass(rs.getString("SOURCE_CLASS"));
		bean.setTargetClass(rs.getString("TARGET_CLASS"));
		bean.setIdUploadFile(rs.getLong("ID_UPLOAD_FILE"));
		try {
			bean.setDateInserted(sdf.parse(rs.getString("DATE_INSERTED")));
			bean.setLastUpdate(sdf.parse(rs.getString("LAST_UPDATE")));
		} catch (ParseException e) {
			LOG.error(e.getMessage());
		}
		bean.setStatus(rs.getString("STATUS"));
		bean.setRetries(rs.getInt("RETRIES"));
		bean.setMetaData(MetadataFieldsDAO.getInstance().selectByID(bean.getID()));
	}

	public void resetAllProcessingRecords() {
		String sql = "UPDATE WORKING_DOCUMENTS SET STATUS = '" + WorkingDocumentBean.STATUS_NEW + "', ERROR_DESCRIPTION=''";
		PreparedStatement pstmt = null;
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			LOG.debug("end update resetAllProcessingRecords");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmt.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
	}

	public int selectCountByStatus(String status) {
		String selectSql = "SELECT COUNT(*) FROM WORKING_DOCUMENTS WHERE STATUS = ?";
		PreparedStatement pstmtSelect = null;
		ResultSet rsSelect = null;
		int returnValue = 0;
		try {
			pstmtSelect = conn.prepareStatement(selectSql);
			pstmtSelect.setString(1, status);
			LOG.debug("START SELECT " + selectSql);
			rsSelect = pstmtSelect.executeQuery();
			rsSelect.next();
			returnValue = rsSelect.getInt(1);
			LOG.debug("END SELECT selectCountByStatus");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmtSelect.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return returnValue;
	}

	public List<WorkingDocumentBean> selectRecordByStatus(String status) {
		String selectSql = "SELECT * FROM WORKING_DOCUMENTS WHERE STATUS = ?";
		PreparedStatement pstmtSelect = null;
		ResultSet rsSelect = null;
		ArrayList<WorkingDocumentBean> returnValue = new ArrayList<WorkingDocumentBean>();
		try {
			pstmtSelect = conn.prepareStatement(selectSql);
			pstmtSelect.setString(1, status);
			LOG.debug("START SELECT " + selectSql);
			rsSelect = pstmtSelect.executeQuery();
			while (rsSelect.next()) {
				WorkingDocumentBean bean = new WorkingDocumentBean();
				populateBeanByResultSet(rsSelect, bean);
				returnValue.add(bean);
			}
			LOG.debug("END SELECT, number of lines found:  " + returnValue.size());
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmtSelect.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return returnValue;
	}

	public boolean existRecord(long idFileUpload, String protocolloGed) {
		String selectSql = "SELECT PROTOCOLLO_GED FROM WORKING_DOCUMENTS WHERE ID_UPLOAD_FILE = ? AND PROTOCOLLO_GED=?";
		PreparedStatement pstmtSelect = null;
		ResultSet rsSelect = null;
		boolean returnValue = false;
		try {
			pstmtSelect = conn.prepareStatement(selectSql);
			pstmtSelect.setLong(1, idFileUpload);
			pstmtSelect.setString(2, protocolloGed);
			LOG.debug("INIZIO SELECT " + selectSql);
			rsSelect = pstmtSelect.executeQuery();
			while (rsSelect.next()) {
				returnValue = rsSelect.getString("PROTOCOLLO_GED") != null ? true : false;
			}
			LOG.debug("Row already present in the DB");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				pstmtSelect.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return returnValue;
	}

	/* Warning: NOT THREAD-SAFE! */
	public int getNextID() {
		Statement st = null;
		ResultSet rsSelect = null;
		int retValue = 0;
		String selectQuery = "select max(id) as MAX_ID from working_documents";
		try {
			st = conn.createStatement();
			rsSelect = st.executeQuery(selectQuery);
			rsSelect.next(); // For HSQL / H2
			retValue = rsSelect.getInt("MAX_ID");
		} catch (SQLException throwables) {
			LOG.error(throwables.getMessage() + " " + throwables.getCause());
		} finally {
			try {
				st.close();
				rsSelect.close();
			} catch (SQLException sqle) {
				LOG.error(sqle.getMessage() + " " + sqle.getCause());
			}
		}
		return ++retValue;
	}

}
