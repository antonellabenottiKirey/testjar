# Update Batch Documenti P8

Obiettivo del software � quello di effettuare un _Change Class_ di documenti gi� presenti sul sistema documentale File Net P8.

## Overview

### Requisiti Tecnici

Il software deve essere parametrico in modo da poter essere riutilizzato in casi analoghi di _Change Class_ massivi. La parametrizzazione deve avvenire tramite file di properties esterno.

Il software deve essere _statefull_ ovvero, qualora si dovesse interrompere l'esecuzione, per una qualunque causa (rete, eccezioni non previste, ecc), si deve poter riprendere l'esecuzione dal punto esatto in cui � stata interrotta.

Essendo le finestre temporali di esecuzione del software molto limitate (3h-4h per volta), ed essendo la mole totale dei documenti importante, il software deve prevedere lotti di esecuzione.

Per gli stessi motivi elencati al punto precedente, � opportuno che il software sia multi-thread, in modo da ottimizzare l'esecuzione. Il numero dei thread deve essere parametrizzato, in modo che possa essere cambiato all'occorrenza.

Gli aggiornamenti sul sistema documentale devono avvenire tramite l'utilizzo di delle API di File Net P8.


### Alimentazione
Il software sar� alimentato da un file csv detto _File Guida_. Questo sar� composto da diverse colonne, quella che identifica univocamente il documento � quella denominata _Protocollo GED_. Il _Protocollo GED_ � quindi la chiave primaria da usare per effettuare gli aggiornamenti sul sistema documentale.
Le altre colonne del _File Guida_ sono _metadati_. I _metadati_ sono fondamentalmente attributi del documento e sono definiti al livello della _Classe Documentale_.

> Trattandosi di un _Change Class_, ci saranno _metadati_ nuovi nella classe target.


### Update
Le API di File Net P8 prevedono due modi per effettuare le _insert_ e gli _update_, e quello da utilizzare in questo caso � con la modalit� _Batch_ che ottimizza i caricamenti massivi.


## Dettaglio

### Parametri
Il file di configurazione, formato _properties_ sar� strutturato in pi� sezioni

#### P8
Tutti i parametri di connessione al sistema documentale (che variano da ambiente ad ambiente)
```properties
P8_OBJECT_STORE_NAME=
P8_USERNAME=
P8_PASSWORD=
P8_URL=
P8_SOURCE_CLASS=
P8_TARGET_CLASS=
P8_DOMAIN_NAME=
P8_STANZA_NAME=
```
> Per il caso specifico l'Object Store di riferimento � `OBJVM04`. La Classe Documentale di partenza � `SinistriEPA` mentre quelle di destinazione possono essere: `SinistriIncarichi` e `SinistriFGVS`


#### File Guida
Percorso assoluto al file guida csv.
```properties

FILE_GUIDA=
```

#### Esecuzione
Parametri di esecuzione del tool
```properties
NUM_THREAD=
DIM_LOTTO=
```

### File Guida
Il file guida rispecchier� questo formato:

```csv
ProtcolloGED;<Nuovo Metadato>
AAA;111
BBB;222
CCC;333

```

> Per il caso specifico il nuovo metadato � `FaseCausa`.


## Note
Dovendo trattare due _Target Class_ diversi, le soluzioni possono essere le seguenti:

1. Il file di properties non conterr� la propriet� `P8_TARGET_CLASS`, e questa verr� definita nel File Guida tramite apposita colonna.

2. Il file properties rimane cosi come specificato, ci saranno due File Guida, uno contente in documenti relativi ad una classe documentale target, l'altro quelli relativi all'altra classe. Le esecuzioni del software avverranno in parallelo con configurazioni diverse, oppure si terminata l'esecuzione del primo File Guida si passa al secondo.


## Build JAR
Eseguire il comando "mvn clean compile assembly:single" per la generazione del file jar